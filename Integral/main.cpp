#include <iostream>
#include <string>
#include "matrix.h"
#include <math.h>

using namespace std;

#define PRECISION 1e-8
#define AMOUNT_POINT 3
#define CUBIC(x) ((x)*(x)*(x))
#define SQR(x) ((x)*(x))
#define PI 3.141592653589793238462
#define NEWTON_PRECISION 1e-8
#define ROOTS_BY_NEWTON 1

//���������� ����� �������� ��������
double *getRootsNewton( mmatrix::Matrix< double> &a_coeff, const double a, const double b );

//���������� ������ �� �������
double *getRootsCardano( mmatrix::Matrix< double> &a_coeff, const double a, const double b );

//���������� ������ � ������������ ���������
double *weightMoments( const double a, const double b, bool Gaussian = true);

//���������� �������� �������� ������
double calcNodesPolynom( double &x, mmatrix::Matrix< double> &a_coeff );

//���������� �������� �������� ������� � �����
double calcFunction( double &x );

#define DEBUG_GAUSSIAN_INTEGRAL 0
//���������� ��������� � ������� ������������ ������ ������
double calcGaussianIntegral(const double &a, const double &b)
{
	double *weights = weightMoments( a, b );

	mmatrix::Matrix< double> w_matrix( AMOUNT_POINT, AMOUNT_POINT );
	mmatrix::Matrix< double> w_b( AMOUNT_POINT, 1 );

	//���������� ������� �����, ��� ���������� ������������� �������� ����������
	for (int s = 0; s < AMOUNT_POINT; ++s)
	{
		for (int i = 0; i < AMOUNT_POINT; ++i)
		{
			w_matrix[s][i] = weights[i + s];
		}
		w_b[s][0] = -weights[AMOUNT_POINT + s];
	}
#if DEBUG_GAUSSIAN_INTEGRAL
	cout << w_matrix << endl << "-----------" << endl;
	cout << w_b << endl << "------------" << endl;
#endif
	//������������ �������� ����������
	mmatrix::Matrix< double> a_coeff = mmatrix::solveSole( w_matrix, w_b );

	//����� �������� ����������
	double *roots;
	try
	{
#if ROOTS_BY_NEWTON
		roots = getRootsNewton( a_coeff, a, b );
#else
		roots = getRootsCardano( a_coeff, a, b );
#endif
	}
	catch (string err)
	{
		throw (err + "__calcGaussianIntegral");
	}


	mmatrix::Matrix< double> A_matrix( AMOUNT_POINT, AMOUNT_POINT );
	mmatrix::Matrix< double> A_b( AMOUNT_POINT, 1 );
	//���������� ������� ��� ���������� ������������� ��� ������������ �������
	for (int s = 0; s < AMOUNT_POINT; ++s)
	{
		for (int i = 0; i < AMOUNT_POINT; ++i)
		{
			A_matrix[s][i] = 1;
			for (int k = 0; k < s; ++k)
			{
				A_matrix[s][i] *= roots[i];
			}
		}
		A_b[s][0] = weights[s];
	}
	//������������ ��� ������������ �������
	mmatrix::Matrix< double> A_coeff = mmatrix::solveSole( A_matrix, A_b );

	double integral = 0;
	//���������� ������������ ���������, �� ������� (25) �� ���������, ����� �� ������ � ����� �� �� ������� ;(
	//��������� ��������� ��� ��� ���, ��� ��� ���� �� ����� ��������� � ������� �� �������, ��� � �������-�������
	//for (int m = 0; m < AMOUNT_POINT * 2; ++m)
	//{
	//	for (int k = 0; k < AMOUNT_POINT; ++k)
	//	{
	//		integral += A_coeff[k][0] * pow( roots[k], m );
	//	}
	//}
	for (int i = 0; i < AMOUNT_POINT; ++i)
	{
		integral += A_coeff[i][0] * calcFunction( roots[i] );
	}

	delete[] weights;
	delete[] roots;

	return integral;
}

//���������� ��������� � ������� ������������ ������ �������-�������
double calcNewtonCotesIntergral( const double &a, const double &b )
{
	double x[3] = {a, (a + b) / 2, b };
	//���������� ��������
	double *w = weightMoments( a, b, false );

	//������������ ������������
	//double a_coeff[3];

	mmatrix::Matrix< double> matrix(3, 3);
	mmatrix::Matrix< double> vector_b( 3, 1 );
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if (i == 0)
				matrix[i][j] = 1;
			if (i == 1)
				matrix[i][j] = x[j];
			if (i == 2)
				matrix[i][j] = x[j] * x[j];
		}
		vector_b[i][0] = w[i];
	}

	mmatrix::Matrix< double> ans  = mmatrix::solveSole( matrix, vector_b );

	//��������
	double integral = 0;
	//���������� ���������
	for (int i = 0; i < AMOUNT_POINT; ++i)
	{
		integral += ans[i][0] * calcFunction( x[i] );
	}

	delete []w;
	return integral;
}

//��������� ������� ��� �������-�������
double complexNewtonKotes( const double &a, const double &b, const double &pieces )
{
	double integral = 0.0;
	double h = (b - a) / pieces;
	double z1, z2;
	z1 = a;
	double i = 1;
	while (i <= pieces)
	{
		z2 = a + i*h;
		integral += calcNewtonCotesIntergral( z1, z2 );
		z1 = z2;
		++i;
	}
	if (i < pieces + 1)
	{
		integral += calcNewtonCotesIntergral( z1, b );
	}

	return integral;
}

//��������� ������� ��� ������
double complexGaussian( const double &a, const double &b, const double &pieces )
{
	double integral = 0.0;
	double h = (b - a) / pieces;
	double z1, z2;
	z1 = a;
	double i = 1;
	while (i <= pieces)
	{
		z2 = a + i*h;
		try
		{
			integral += calcGaussianIntegral( z1, z2 );
		}
		catch (string err)
		{
			cout << err << endl;
			return EXIT_FAILURE;
		}
		z1 = z2;
		++i;
	}
	if (i < pieces + 1)
	{
		integral += calcGaussianIntegral( z1, b );
	}

	return integral;
}

//��� ��� ��������� �� �������
typedef double( *calcIntegralPT )(const double &, const double &, const double &);
enum  MethodI
{
	GAUSSIAN = 0, NEWTON_COTESS
};
//������� ������� 
double processEitken( const double &a, const double &b, MethodI method, const double &AST)
{
	calcIntegralPT calcIntegral[2] = { complexGaussian, complexNewtonKotes };
	double *integral = new double[3];
	int step = 2;
	__int64 pieces = 1;
	double m = 0;
	double prev_m = -1;
	int iter = 12;
	double temp = 0;

	integral[1] = calcIntegral[method]( a, b, static_cast< double> (pieces) );
	integral[2] = calcIntegral[method]( a, b, static_cast< double> (pieces)* step );

	do
	{
		//���������� ����������
		integral[0] = integral[1];
		integral[1] = integral[2];
		integral[2] = calcIntegral[method]( a, b, static_cast< double> (pieces) * step *step );
	
		pieces *= step;

		if (integral[0] == EXIT_FAILURE || integral[1] == EXIT_FAILURE	 || integral[2] == EXIT_FAILURE)
		{
			cout << "Gaussian integral failure...On step = " << pieces << endl;
			cout << "Speed == " << m << endl;
			--iter;
			break;
		}
		
		//TODO: ���������, ����� �������� ���� �� �� ������������� ��������, � �� ������� �����, ��� � ������� ���������
		prev_m = m;
		m = (integral[2] - integral[1]) / (integral[1] - integral[0]);
		m = log( m ) / log( 1./2. );
		
		cout << "Speed = " << m << " On step = " << pieces << endl;
		
		--iter;
		temp = (integral[2] - integral[1]) / (pow( step, AST + 1 ) - 1);
	} while ((abs (temp) >= PRECISION) && (iter != 0));

	delete[]integral;
	return m - 1;
}

//������� ��������� � ������� � ������� ������� �� ��� ������ �����, � ������� ����������� ����� ���� ����� AST
double calcComplexIntegralAst( const double &a, const double &b, MethodI method, const double &AST)
{
	calcIntegralPT calcIntegral[2] = { complexGaussian, complexNewtonKotes };
	double *temp_integral = new double[2] { 0, 0 };
	double integral = 0;
	double prev_integral = 0;
	__int64 step = 2;
	__int64 pieces = 2;
	int iter = 14;

	double epsilon = 1e-8;

	double temp = 0;

	temp_integral[0] = calcIntegral[method]( a, b, static_cast< double> (pieces) );
	temp_integral[1] = calcIntegral[method]( a, b, static_cast< double> (pieces) * step );

	double H_step = (b - a) / pieces;

	while (abs( (temp_integral[0] - temp_integral[1]) / (pow( step, AST ) - 1) ) >= PRECISION && iter)
	{
		if (temp_integral[0] == EXIT_FAILURE || temp_integral[1] == EXIT_FAILURE)
		{
			cout << "Gaussian integral failure...On step = " << pieces << endl;
			break;
		}

		H_step = H_step*pow( (epsilon*(1 - pow( 2, -AST )) / abs( temp_integral[0] - temp_integral[1] )), 1. / AST );
		
		long H_temp = int( 1. / H_step ) + 1;

		cout << " H_step  =  " << H_temp << endl;

		H_temp += (H_temp % 2 == 0 ? 0 : 1);

		temp_integral[0] = calcIntegral[method]( a, b, static_cast< double> (H_temp / 2) );
		temp_integral[1] = calcIntegral[method]( a, b, static_cast< double> (H_temp));

		if (temp_integral[0] == EXIT_FAILURE || temp_integral[1] == EXIT_FAILURE)
		{
			cout << "Gaussian integral failure...On step = " << pieces << endl;
			break;
		}
		iter--;
	}

	integral = temp_integral[1];
	delete[]temp_integral;
	return integral;
}

#define DEBUG_COMPLEX_INTEGRAL 0
//��������� �������
double calcComplexIntegral( const double &a, const double &b, MethodI method, const int &AST)
{
	calcIntegralPT calcIntegral[2] = { complexGaussian, complexNewtonKotes };
	double integral = 0;
	double prev_integral = 0;
	__int64 step = 2;
	__int64 pieces = 1;

	double temp = 0;
	do
	{
		prev_integral = integral;
		//���������� ���������
		integral = calcIntegral[method]( a, b, static_cast< double> (pieces) );
		if (integral == EXIT_FAILURE)
		{
			cout << "Gaussian integral failure...On step = " << pieces  << endl;
			pieces *= step;
			break;
		}

		temp = (integral - prev_integral) / (pow( step,  AST + 1) - 1);
		pieces *= step;
#if DEBUG_COMPLEX_INTEGRAL
		cout << integral << endl;
#endif
	} while (abs( temp) >= PRECISION);

	cout << " Step = " << pieces << endl;

	return integral;
}

//FINDING ROOTS BY CARDANO
double *getRootsCardano( mmatrix::Matrix< double> &a_coeff, const double a, const double b )
{
	double Q = (-3 * a_coeff[1][0] + SQR(a_coeff[0][0])) / (9);
	double R = (2 * CUBIC( a_coeff[0][0] ) - 9 * a_coeff[0][0] * a_coeff[1][0] + 27*a_coeff[2][0]) / 54;

	if ( SQR( R) >= CUBIC( Q))
	{
		throw string( "Bad roots__getRootsCardano" );
	}

	double *roots = new double[3];
	double t = acos( R / sqrt( CUBIC( Q))) / 3;
	roots[0] = -2 * sqrt( Q )*cos( t ) - a_coeff[0][0] / 3;
	roots[1] = -2 * sqrt( Q )*cos( t + (2 * PI / 3) ) - a_coeff[0][0] / 3;
	roots[2] = -2 * sqrt( Q )*cos( t - (2 * PI / 3) ) - a_coeff[0][0] / 3;

	for (int i = 0; i < 3; ++i)
	{
		if (roots[i] > b || roots[i] < a)
		{
			throw string( " Bad roots__getRootsCardano" );
		}
	}

	return roots;
}
// \FINDING ROOTS BY CARDANO

// FINDING ROOTS BY NEWTON
//first derivative Nodes Polynom
double calcNodesPolynom_deriv( double x, mmatrix::Matrix< double> &a_coeff )
{
	return 3 * x*x + 2 * a_coeff[2][0]*x + a_coeff[1][0];
}

//Nodes polynom 3 points
double calcNodesPolynom( double &x, mmatrix::Matrix< double> &a_coeff )
{
	return x*x*x + a_coeff[2][0] * x*x + a_coeff[1][0] * x + a_coeff[0][0];
}

#define DEBUG_SIMPLENEWTON 0
//find first root for our polynom
double simpleNewton( double start_x, mmatrix::Matrix< double> &a_coeff, double precision = NEWTON_PRECISION )
{
	double current_x = start_x, prev_x = 0, dx = precision + 1;
	int max_iter = 1000;
	int iter = 0;

	while (dx >= precision && iter < max_iter)
	{
		prev_x = current_x;
		current_x = prev_x - calcNodesPolynom( prev_x, a_coeff ) / calcNodesPolynom_deriv( prev_x, a_coeff );
		dx = abs( current_x - prev_x );
#if DEBUG_SIMPLENEWTON
		cout << " x = " << current_x << endl;;
		cout << "dx = " << dx << endl;
		cout << "F(x) = " << calcNodesPolynom( current_x, a_coeff );
#endif
		iter++;
	}

	if (iter == max_iter)
	{
		throw string( "roots not found__SimpleNewton" );
	}

	return current_x;
}

//find roots for polynom
double *getRootsNewton( mmatrix::Matrix< double> &a_coeff, const double a, const double b )
{
	double *roots = new double[3];

	try
	{
		roots[0] = simpleNewton( b, a_coeff );
		if (roots[0] < a || roots[0] > b)
		{
			throw string( "Roots out of range__simpleNewton__getRootsNewton" );
		}
	}
	catch (string err)
	{
		throw err + "__getRootsNewton";
	}

	double coeff_b = (a_coeff[2][0] + roots[0]);
	double coeff_c = (a_coeff[1][0] + roots[0] * (a_coeff[2][0] + roots[0]));

	double D = coeff_b*coeff_b - 4 * coeff_c;

	if (D < 0 || (roots[0] < a || roots[0] > b))
	{
		throw string("Bad roots__getRootsNewton");
	}

	roots[1] = (-coeff_b + sqrt( D )) / 2;
	roots[2] = (-coeff_b - sqrt( D )) / 2;

	if (roots[1] < a || roots[1] > b || roots[2] < a || roots[2] > b)
	{
		throw string( "Bad roots__getRootsNewton" );
	}

	return roots;
}
// \FINDING ROOTS BY NEWTON

int main()
{
	double a = 1.3, b = 2.2;
	//double a = 1, b = 3;
	//Calculate integral for 4 * cos( 0.5*x )*exp( -(5 * x) / 4 ) + 2 * sin( 4.5*x )*exp( x/8 ) + 2 on [1.3, 2.2]
	cout << "       _ 2.2 " << endl;
	cout << "      /    " << endl;
	cout << "      |  " << endl; 
	cout << "F(x)= | 4cos(x/2)*e^( -5*x/4) + 2sin( 4.5*x)*e^( x/8) + 2) dx" << endl;
	cout << "      |  " << endl;
	cout << "      |  " << endl;
	cout << "     _/    " << endl;
	cout << "    1.3 " << endl;

	cout << "simple NewtonCotess: F(x) = " << calcNewtonCotesIntergral( a, b ) << endl;

	try
	{
		cout << "simple GaussianCotess: F(x) = " << calcGaussianIntegral( a, b ) << endl;
	}
	catch (string err)
	{
		cout << err << endl;
	}

	cout << "Complex Gaussian with Runge Rule: F(x) = " << setprecision( 16 ) << calcComplexIntegral( a, b, GAUSSIAN, 5 ) << endl;
	cout << "Complex Newton-Kotes with Runge Rule: F(x) = " << calcComplexIntegral( a, b, NEWTON_COTESS, 2 ) << endl;

	cout << "-----------------------------" << endl;
	
	cout << "Eitken process for Gaussian..." << endl;
	double AST_Gaussian = processEitken( a, b, GAUSSIAN, 6 );
	cout << "Eitken process for Gaussian  speed = " << AST_Gaussian << endl;
	cout << "-----------------------------" << endl;
	
	cout << "Eitken process for Newton-Kotess..." << endl;
	double AST_NewtonKotess = processEitken( a, b, NEWTON_COTESS, 3 );
	cout << "Eitken process for Newton-Kotess  speed = " << AST_NewtonKotess << endl;
	cout << "-----------------------------" << endl;
	
	cout << "Complex Gaussian with optimal step by AST = " << calcComplexIntegralAst( a, b, GAUSSIAN, 6 ) << endl;
	cout << "Complex Newton-Kotess with optimal step by AST = " << calcComplexIntegralAst( a, b, NEWTON_COTESS, 3 ) << endl;
	
	cout << "-----------------------------" << endl;
	cout << "Complex Gaussian integral with optimal step by Eitken = " << calcComplexIntegralAst( a, b, GAUSSIAN, AST_Gaussian + 1) << endl;
	cout << "Complex Newton-Kotess integral with optimal step by Eitken = " << calcComplexIntegralAst( a, b, NEWTON_COTESS, AST_NewtonKotess + 1) << endl;

	return EXIT_SUCCESS;
}


//���������� �������� ��� ������� p(x) = (x - 1.3)^(0)*(2.2 - x)^(-5/6)
double* weightMoments( const double down, const double up, bool Gaussian )
{
	double alpha = 5.0 / 6.0;
	double b = 2.2;

	double moment_0 = -6. * pow( 11. / 5 - up, 1. / 6 ) - (-6. * pow( 11. / 5 - down, 1. / 6 ));
	double moment_1 = -(6. * (5. * up + 66.)*pow(11. / 5 - up, 1. / 6)) / 35. - (-(6. * (5. * down + 66.)*pow(11. / 5 - down, 1. / 6)) / 35.);
	double moment_2 = -(6 * pow( 11. / 5 - up, 1. / 6. )*(175. * up*up + 660. * up + 8712.)) / 2275. - (-(6 * pow( 11. / 5 - down, 1. / 6. )*(175. *down*down + 660. * down + 8712.)) / 2275.);
						  
	double *ans;
	if (Gaussian)
	{
		double moment_3 = -(6. * pow( 11. / 5. - up, 1. / 6. )*(11375. * up *up*up + 34650. * up*up + 130680. * up + 1724976.)) / 216125.;
		moment_3 = moment_3 - (-(6. * pow( 11. / 5. - down, 1. / 6. )*(11375. * down *down*down + 34650. * down*down + 130680. * down + 1724976.)) / 216125.);
		double moment_4 = -(6. * pow( 11. / 5. - up, 1. / 6. )*(43225. * up * up * up * up + 120120. * up*up*up + 365904. * up*up + (6899904. * up) / 5. + 455393664. / 25.)) / 1080625.;
		moment_4 = moment_4 - (-(6. * pow( 11. / 5. - down, 1. / 6. )*(43225. * down * down * down * down + 120120. * down*down*down + 365904. * down*down + (6899904. * down) / 5. + 455393664. / 25.)) / 1080625.);
		double moment_5 = -(6. * pow( 11. / 5. - up, 1. / 6. )*(5403125. * up*up*up*up*up + 14264250. * up * up * up * up + 39639600. * up * up * up + 120748320. * up * up + 455393664. * up + 30055981824. / 5.)) / 167496875.;
		moment_5 = moment_5 - (-(6. * pow( 11. / 5. - down, 1. / 6. )*(5403125. * down*down*down * down * down + 14264250. * down * down * down * down + 39639600. * down * down * down + 120748320. * down * down + 455393664. * down + 30055981824. / 5.)) / 167496875.);

		ans = new double[6];
		ans[3] = moment_3;
		ans[4] = moment_4;
		ans[5] = moment_5;
	}
	else
	{
		ans = new double[3];
	}
	ans[0] = moment_0;
	ans[1] = moment_1;
	ans[2] = moment_2;

	return ans;
}

//���������� �������� �������� ������� � �����
double calcFunction( double &x )
{
	return ( 4.0 * cos( 0.5*x )*exp( -(5.0 * x) / 4.0 ) + 2.0 * sin( 4.5*x )*exp( x / 8.0 ) + 2.0 ) /*/ pow( 2.2 - x, 5/6 ) */;
}

