#include <iostream>
#include <ctime>
#include <ppl.h>
#include <iomanip>
using namespace std;

namespace mmatrix
{
template<class T_elem = double> 
class Matrix
{
	int n, m;
	int exchanges; //for P or Q, describes number of row or cols that was exchanged
	int rank;
	bool isTriangle;
	bool isSquare;
	T_elem **table;
public: 
	//constructors
	Matrix< T_elem>(int _n = 1, int _m = 1) /*: n(_n), m(_m)  */
	{
		n = _n;
		m = _m;
		exchanges = 0;

		isTriangle = false;
		isSquare = (n == m) ? true : false;
		
		rank = n;

		table = new T_elem*[n];
		for (int i = 0; i < n; ++i)
		{
			table[i] = new T_elem[m];
			memset (table[i], 0, m*sizeof(T_elem));
		}
	};

	Matrix< T_elem>(const Matrix<T_elem> &matrix)
	{
		this->m = matrix.m;
		this->n = matrix.n;

		isTriangle = matrix.isTriangle;
		isSquare = matrix.isSquare;
		isSquare = (n == m) ? true : false;
		rank = matrix.rank;

		table = new T_elem*[n];
		for (int i = 0; i < n; ++i)
		{
			table[i] = new T_elem[m];
		}

		for (int i = 0; i < n; ++i)
			for (int j = 0; j < m; ++j)
			{
				this->table[i][j] = matrix.table[i][j];
			}
	}
	
	//methods
	void CreateMatrix(int _n, int _m)
	{
		n = _n;
		m = _m;
		exchanges = 0;
		rank = n;

		isTriangle = false;
		isSquare = (n == m) ? true : false;

		table = new T_elem*[n];
		for (int i = 0; i < n; ++i)
		{
			table[i] = new T_elem[m];
			memset (table[i], 0, m*sizeof(T_elem));
		}
	}
	Matrix< T_elem> transpose()
	{
		Matrix <T_elem> temp(this->m, this->n);

		for (int i = 0; i < this->m; ++i)
		{
			for (int j = 0; j < this->n; ++j)
			{
				temp.table[i][j] = this->table[j][i];
			}
		}

		return temp;
	}

	void fillRandom()
	{
		isTriangle = false;
		isSquare = (n == m) ? true : false;

		table = new T_elem*[n];
		for (int i = 0; i < n; ++i)
		{
			table[i] = new T_elem[m];
			memset (table[i], 0, m*sizeof(T_elem));
		}

		srand(time(0));
		 for( int  i = 0 ; i < n ; ++i)
			for(int j = 0 ;  j < m ; ++j)
				table[i][j] = rand()%100;
	}

	void SwapRows( int _i, int _j)
	{
		if (_i == _j)
		{
			return;
		}
        swap( this->table[_i], this->table[_j] );
		//for (int i = 0; i < this->m; ++i)
		//{
		//	swap(this->table[_i][i], this->table[_j][i]);
		//}
	}

	void SwapCols (int _i, int _j)
	{
		if (_i == _j)
		{
			return;
		}

		for (int i = 0; i < this->n; ++i)
		{
			swap(this->table[i][_i], this->table[i][_j]);
		}
	}

	Matrix< T_elem> IdentityMatrix(int _n)
	{
		Matrix < T_elem> result(_n, _n);
	
		for (int i = 0; i < _n; ++i)
			result[i][i] = 1;

		return result;
	}


	//operators
	T_elem* operator[] (int _i)
	{
		if (_i >= n)
		{
			cout << "Index out of range__operator[]" << endl;
		}
		return table[_i];
	}

	Matrix< T_elem> operator- (Matrix< T_elem>& matrix)
	{
		Matrix< T_elem> ans( matrix.n, matrix.m);

		for (int i = 0; i < matrix.n; ++i)
		{
			for (int j = 0; j < matrix.m; ++j)
			{
				ans[i][j] = -matrix[i][j];
			}
		}

		return ans;
	}

	Matrix < T_elem> & operator=  (Matrix < T_elem> rightnewMatrix)
	{	
		for (int i = 0; i < n; ++i)
		{
			delete []table[i];
		}
		delete []table;

		this->m = rightnewMatrix.m;
		this->n = rightnewMatrix.n;

		this->isTriangle = rightnewMatrix.isTriangle;
		this->isSquare = (n == m) ? true : false;

		this->exchanges = rightnewMatrix.exchanges;
		this->rank = rightnewMatrix.rank;

		table = new T_elem*[n];
		for (int i = 0; i < n; ++i)
		{
			table[i] = new T_elem[m];
		}

		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < m; ++j)
			{
				this->table[i][j] = rightnewMatrix.table[i][j];
			}
		}

		return *this;
	}

	//friennds methods
	template< typename T_elem> 
	friend ostream& operator << (ostream&, Matrix<T_elem>&);

	template< typename T_elem> 
	friend istream& operator >> (istream&, Matrix<T_elem>&);

	template< typename T_elem> 
	friend Matrix< T_elem> operator+ (Matrix< T_elem>&, Matrix< T_elem>&);

	template< typename T_elem> 
	friend Matrix< T_elem> operator- (Matrix< T_elem>&, Matrix< T_elem>&);

	template< typename T_elem>
	friend Matrix< T_elem> operator-(const Matrix< T_elem>&);

	template< typename T_elem>
	friend Matrix< T_elem> operator* (Matrix< T_elem>&, Matrix< T_elem>&);

	template< typename T_elem>
	friend bool operator== (Matrix< T_elem>&, Matrix< T_elem>&);

	template< typename T_elem>
	friend T_elem dot (Matrix< T_elem>&, Matrix< T_elem>&);


	~Matrix()
	{
		if (this == 0)
			return;
		for (int i = 0; i < n; ++i)
		{
			delete []table[i];
		}
		delete []table;
	}

	//getters or setters
	int getN ()
	{
		return n;
	}
	int getM ()
	{
		return m;
	}
	void setN(int _n)
	{
		n=_n;
	}
	void setM(int _m)
	{
		m=_m;
	}
	void setRank(int _rank)
	{
		rank = _rank;
	}
	int getRank()
	{
		return rank;
	}
	bool IsTriangle ()
	{
		return isTriangle;
	}
	void setIsTriangle (bool _isTriangle)
	{
		isTriangle = _isTriangle;
	}
	bool IsSquare ()
	{
		return isSquare;
	}
	void setIsSquare (bool _isSquare)
	{
		isSquare = _isSquare;
	}
	void addExchange ()
	{
		++exchanges;
	}
	int getExchanges ()
	{
		return exchanges;
	}

};

const Matrix< > NullMatrix = Matrix< >(0, 0);
Matrix< > GetNullMatrix ()
{
	Matrix<> ans = NullMatrix;

	return ans;
}

//Matrix< >& NullMatrix()
//{
//	Matrix< > *nullnewMatrix;
//	nullnewMatrix = new Matrix< >(0, 0);
//	return *nullnewMatrix;
//}

template<typename T_elem>
ostream& operator<<(ostream& os, Matrix<T_elem>& outMatrix) 
{
	for (int i = 0; i < outMatrix.n; ++i)
	{
		for (int j = 0; j < outMatrix.m - 1; ++j)
			os << setw(9) << fixed << outMatrix.table[i][j] << " ";
		os << setw(9) << fixed << outMatrix.table[i][outMatrix.m - 1] << std::endl;
	}
   return os;
}

template<typename T_elem>
istream& operator>>(istream& is, Matrix<T_elem>& inMatrix) 
{
	if (inMatrix.n != 0)
	{
		for (int i = 0; i < inMatrix.n; ++i)
		{
			delete []inMatrix.table[i];
		}
		delete []inMatrix.table;
	}
	int n, m;
	is >> n >> m;

	inMatrix.n = n;
	inMatrix.m = m;

	inMatrix.isTriangle = false;
	inMatrix.isSquare = (n == m) ? true : false;
	inMatrix.rank = n;
	inMatrix.exchanges = 0;

	inMatrix.table = new T_elem*[n];
	for (int i = 0; i < n; ++i)
	{
		inMatrix.table[i] = new T_elem[m];
		memset (inMatrix.table[i], 0, m*sizeof(T_elem));
	}

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < m; ++j)
		{
			is >> inMatrix[i][j];
		}
	}

	return is;
}

Matrix< double> IdentityMatrix(int _n)
{
	Matrix < double> result(_n, _n);
	
	result.setIsTriangle(true);
	result.setIsSquare(true);

	for (int i = 0; i < _n; ++i)
		result[i][i] = 1;

	return result;
}

template< typename T_elem>
Matrix<T_elem> operator+ (Matrix< T_elem>& matrix_1, Matrix< T_elem>& matrix_2)
{
	if (matrix_1.n < matrix_2.n || matrix_1.m < matrix_2.m)
	{
		return matrix_1 + matrix_2;
	}

	Matrix< T_elem> ans(matrix_1.n, matrix_1.m);

	for (int i = 0; i < matrix_2.n; ++i)
	{
		for (int j = 0; j < matrix_2.m; ++j)
		{
			ans.table[i][j] = matrix_1.table[i][j] + matrix_2.table[i][j];
		}
	}

	return ans;
}

template<typename T_elem> 
Matrix<T_elem> operator- (const Matrix< T_elem> &_matrix)
{
	Matrix< T_elem> matrix = _matrix;

	for (int i = 0; i < matrix.n; ++i)
	{
		for (int j = 0; j < matrix.m; ++j)
		{
			matrix[i][j] = -matrix[i][j];
		}
	}

	return matrix;
}


template<typename T_elem> 
Matrix<T_elem> operator- (Matrix< T_elem>& matrix_1, Matrix< T_elem>& matrix_2)
{
	if (matrix_1.n < matrix_2.n || matrix_1.m < matrix_2.m)
	{
		return matrix_1 + matrix_2;
	}

	Matrix< T_elem> ans(matrix_1.n, matrix_1.m);

	for (int i = 0; i < matrix_2.n; ++i)
	{
		for (int j = 0; j < matrix_2.m; ++j)
		{
			ans.table[i][j] = matrix_1.table[i][j] - matrix_2.table[i][j];
		}
	}

	return ans;
}

//������� ���������� ������ �� �������!!!
template< typename T_elem>
Matrix< T_elem> operator* (Matrix< T_elem>& matrix_1, Matrix< T_elem>& matrix_2)
{
	if (matrix_1.m != matrix_2.n)
	{
		cout << "Matrices are inconsistent!__operator*" << endl;
		exit(1);
	}

	Matrix< T_elem> ans(matrix_1.n, matrix_2.m);

	for (int i = 0; i < ans.n; ++i)
	{
		for (int j = 0; j < ans.m; ++j)
		{
			for (int k = 0; k < matrix_1.m; ++k)
			{
				ans.table[i][j] += matrix_1.table[i][k] * matrix_2.table[k][j];
			}
		}
	}

	return ans;
}

////parrallel multiplicataion
//template< typename T_elem>
//Matrix< T_elem> operator* (Matrix< T_elem>& matrix_1, Matrix< T_elem>& matrix_2)
//{
//	if (matrix_1.m != matrix_2.n)
//	{
//		cout << "Matrices are inconsistent!__operator*" << endl;
//		exit(1);
//	}
//
//	Matrix< T_elem> ans(matrix_1.n, matrix_2.m);
//
//	Concurrency::parallel_for(( int) 0, ans.n, [&](int i)
//	{
//		for (int j = 0; j < ans.getM(); ++j)
//		{
//			for (int k = 0; k < matrix_1.getM(); ++k)
//			{
//				ans[i][j] += matrix_1[i][k] * matrix_2[k][j];
//			}
//		}
//	});
//
//	return ans;
//}

template< typename T_elem>
bool operator== (Matrix< T_elem>& leftnewMatrix, Matrix< T_elem>& rightnewMatrix) //if (n == n1 && m == m1) then compare else return false
{
	if (leftnewMatrix.isSquare == rightnewMatrix.isSquare && leftnewMatrix.n == rightnewMatrix.n && leftnewMatrix.m == rightnewMatrix.m)
	{
		for (int i = 0; i < rightnewMatrix.n; ++i)
		{
			for (int j = 0; j < rightnewMatrix.m; ++j)
			{
				if (leftnewMatrix[i][j] != rightnewMatrix[i][j])
				{
					return false;
				}
			}
		}
		return true;
	}
	return false;
}

//return dot-product is possible else return 0
template< typename T_elem>
T_elem dot( Matrix< T_elem> &matrix_1, Matrix< T_elem> &matrix_2)
{
	if (matrix_1.n != matrix_2.n || matrix_1.m != matrix_2.m)
	{
		return 0.0;
	}

	double ans = 0;
	for (int i = 0; i < matrix_1.n; ++i)
	{
		for (int j = 0; j < matrix_2.m; ++j)
		{
			ans += matrix_1[i][j] * matrix_2[i][j];
		}
	}

	return ans;
}

template< typename T_elem> 
T_elem GaussianDeterminant ( Matrix < T_elem> matrix)
{
	T_elem pivotValue = 0;
	T_elem det = 1;
	int n = matrix.getN();
	for( int i = 0; i < n; i++ ) {
		pivotValue = 0;
		int pivotRow = -1, pivotCol = -1;

		for (int row = i; row < n; ++row)
		{
			for (int col = i; col < n; ++col)
			{
				if ( abs(matrix[row][col]) > pivotValue) {
					pivotValue = abs( matrix[row][col]);
					pivotRow = row;
					pivotCol = col;
				}
			}
		}

		if (pivotValue == 0)
		{
			cout << "Matrix is singular__GaussianDeterminant" << endl;
			return 0;
		}

		matrix.SwapRows(pivotRow, i);
		matrix.SwapCols(pivotCol, i);

		det *= matrix[i][i];

		for (int j = i + 1; j < n; ++j)
		{
			matrix[i][j] /= matrix[i][i];
		}
		matrix[i][i] = 1;

		for (int j = i + 1; j < n; ++j)
		{
			for (int k = i + 1; k < n; ++k)
			{
				matrix[j][k] -= matrix[j][i]*matrix[i][k];
			}
			matrix[j][i] = 0;
		}
	}

	return det;
}

template< typename T_elem>
T_elem MatrixDeterminant( Matrix < T_elem> &matrix )
{
	if ( ! matrix.IsSquare())
	{
		cout << "Matrix is not Square!__determinant" << endl;
	}
	if (matrix.IsTriangle())
	{
		T_elem ans = 1;
		for (int i = 0; i < matrix.getN(); ++i)
		{
			ans *= matrix[i][i];
		}
		return ans;
	}

	return GaussianDeterminant (matrix);
}


template <class T_elem> 
double CalculateConditionalNumber (Matrix< T_elem> &A, Matrix< T_elem> &InvA)
{
	double sum_a = 0, sum_inva = 0;
	int n = A.getN();

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			sum_a += A[i][j]*A[i][j];
			sum_inva += InvA[i][j]*InvA[i][j];
		}
	}

	return sqrt (sum_a / sum_inva);
}

template <class T_elem> 
double CalculateDeterminant (Matrix< T_elem> &P, Matrix< T_elem> &U, Matrix< T_elem> &Q)
{
	return ( (Q.getExchanges() + P.getExchanges()) % 2 ? -1 : 1 )*MatrixDeterminant(U);
}

template <class T_elem> 
Matrix< T_elem> CalculateInvertibleMatrix (Matrix< T_elem> &L, Matrix< T_elem> &U, Matrix< T_elem> &P, Matrix< T_elem>& Q = GetNullMatrix())
{
	int n = L.getN();
	Matrix< T_elem> X(n, n);
	Matrix< T_elem> E(n, 1);
	Matrix< T_elem> t_vector;

	for (int i = 0; i < n; ++i)
	{
		E[i][0] = 1;
		t_vector = ResolveSOLE_LUP(L, U, P, E, Q);
		for (int j = 0; j < n; ++j)
		{
			X[j][i] = t_vector[j][0];
		}
		E[i][0] = 0;
	}

	return X;
}

//return 0 if correct decomposition or 1 if matrix is singular
template <class T_elem>
int LUP(Matrix< T_elem> &A, Matrix< T_elem> &L, Matrix< T_elem> &U, Matrix< T_elem> &P, int &AMOUNT, Matrix< T_elem> &Q = GetNullMatrix())
{
	int isSingular = 0;
	Matrix< T_elem> C;
	if (Q == GetNullMatrix())
		isSingular = LUP_C (A, C, P);
	else
		isSingular = LUPQ_C (A, C, P, Q, AMOUNT);

	L = IdentityMatrix(C.getN());
	U = L;

	for (int i = 0; i < C.getN(); ++i)
	{
		for (int j = 0; j < C.getN(); ++j)
		{
			if (j < i)
			{
				L[i][j] = C[i][j];
			}
			else
			{
				U[i][j] = C[i][j];
			}
		}
	}
	U.setRank(C.getRank());
	U.setIsTriangle(true);
	L.setRank(C.getRank());
	L.setIsTriangle(true);
	return isSingular;
}

//return 0 if correct decomposition or 1 if matrix is singular
template <class T_elem>
int LUP_C( Matrix< T_elem> &A, Matrix< T_elem> &C, Matrix< T_elem> &P) 
{
	//������ �������
	const int n = A.getN();
 
	C = A;

	//������� ������������
	P = IdentityMatrix(n);

	double pivotValue = 0;
	int pivotRow = -1;
	for( int i = 0; i < n; i++ ) {
		pivotValue = 0;
		pivotRow = -1;

		//����� �������� �������� ������������� �� ������� �������
		for( int row = i; row < n; ++row ) {
			if( abs(C[ row ][ i ]) > pivotValue ) {
				pivotValue = abs(C[ row ][ i ]);
				pivotRow = row;
			}
		}

		//�������� ������� �� �������������
		if (pivotValue == 0)
		{
			cout << "Matrix is singular__LUP_C" << endl << " Rank == " << i << endl;
			C.setRank(i);
			return 1;
		}

		//������ ������� i-� ������ � ������ � ������� ���������
		P.SwapRows(pivotRow, i);
		C.SwapRows(pivotRow, i);

		for( int j = i + 1; j < n; ++j ) {
			//�������� �� ���������� ������� � i-�� ������� � ����� �������� �� ������� �������
			C[j][i] /= C[i][i];
			//�� ������� �������� j-� ������ �������� ������������ ������� �������� ���� ������ � �������� ����� ������� � i-� ������
			for( int k = i + 1; k < n; ++k ) 
				C[j][k] -= C[j][i] * C[i][k];
		}
	}

	return 0;
}

//return 0 if correct decomposition or 1 if matrix is singular
template <class T_elem>
int LUPQ_C( Matrix< T_elem> &A, Matrix< T_elem> &C, Matrix< T_elem> &P, Matrix< T_elem> &Q, int &AMOUNT) 
{
	//������ �������
	const int n = A.getN();
 
	C = A;

	//������� ������������
	Q = IdentityMatrix(n), P = IdentityMatrix(n);

	double pivotValue = 0, pivotRatio = 0;
	int pivotRow = -1, pivotCol = -1;
	for( int i = 0; i < n; i++ ) {
		pivotValue = 0;
		pivotRow = -1, pivotCol = -1;

		//����� �������� �������� �� ����� ����������� ����� �������, ��� ������������ �������� ���������� ������ ������� ��� ������������ ;(
		for (int row = i; row < n; ++row)
		{
			for (int col = i; col < n; ++col)
			{
				if ( abs(C[row][col]) > pivotValue) {
					pivotValue = abs(C[row][col]);
					pivotRow = row;
					pivotCol = col;
				}
			}
		}

		//�������� ������� �� �������������
		if (pivotValue == 0)
		{
			cout << "Matrix is singular__LUP_C" << endl << " Rank == " << i << endl;
			C.setRank( i);
			return 1;
		}

		//������ ������� i-� ������ � ������ � ������� ���������
		P.SwapRows(pivotRow, i);
		P.addExchange();
		C.SwapRows(pivotRow, i);

		Q.SwapCols(pivotCol, i);
		Q.addExchange();
		C.SwapCols(pivotCol, i);


		for( int j = i + 1; j < n; ++j ) {
			//�������� �� ���������� ������� � i-�� ������� � ����� �������� �� ������� �������
			C[j][i] /= C[i][i];
			AMOUNT += 1;
			//�� ������� �������� j-� ������ �������� ������������ ������� �������� ���� ������ � �������� ����� ������� � i-� ������
			for( int k = i + 1; k < n; ++k ) 
			{
				C[j][k] -= C[j][i] * C[i][k];
				AMOUNT += 2;
			}
		}
	}

	return 0;
}

//return vector-answer, withot multiplication U and Q and if Matrix is inconsistent return NullMatrix
template <class T_elem>
Matrix< T_elem> ResolveSOLE_LUP (Matrix< T_elem> &L, Matrix< T_elem> &U, Matrix< T_elem> &P, Matrix< T_elem> b, int &AMOUNT, Matrix< T_elem>& Q = GetNullMatrix())
{
	//firstly we must solve Ly = Pb, so...
	//P*b
	b = P*b;

	int n = b.getN();
	//������ ������ ������ ��������
	Matrix < T_elem> y(n, 1);

	//cout << L << endl << "----------" << endl;
	y[0][0] = b[0][0];
	//cout << U << endl << "----------" << endl;
	//cout << L << endl << "----------" << endl;
	//cout << b << endl << "------------" << endl;
	for (int i = 1; i < n; ++i)
	{
		y[i][0] = b[i][0];
		for (int j = 0; j < i; ++j)
		{
			y[i][0] -= y[j][0]*L[i][j];
			AMOUNT += 2;
		}
	}

	//secondly we must solve Ux = y by backward substitution
	Matrix < T_elem> x(n, 1);

	//cout << U << endl << "----------" << endl;
	if (U.getRank() != U.getN())
	{
		//cout << y << endl << "----------" << endl;

		for (int i = n - 1; i >= U.getRank(); --i)
		{
			if ( y[i][0] != 0 )
				return GetNullMatrix ();
			else
				x[i][0] = 1;
		}
	}
	else
	{
		x[ n - 1][0] = y[n - 1][0] / U[ n - 1][ n - 1];
		AMOUNT += 1;
	}
	//cout << x << endl << "----------" << endl;

	for (int i = U.getRank() - 1; i >= 0; --i)
	{
		//cout << U << endl << "----------" << endl;

		//cout << x << endl << "----------" << endl;
		//cout << y << endl << "----------" << endl;
		for (int j = n - 1; j > i; --j)
		{
			y[i][0] -= x[ j][ 0]*U[ i][ j];
			AMOUNT += 2;
			//cout << y << endl << "----------" << endl;
		}
		x[ i][0] = y[i][0] / U[ i][ i];
		AMOUNT += 1;
	}

	if (! (Q == GetNullMatrix()) )
	{
		//cout << Q << endl << "----------" << endl;
		//cout << x << endl << "----------" << endl;
		x =  Q*x;
		//cout << x << endl << "----------" << endl;
	}
	return x;
}

//return invers matrix if it's possible otherwise return NullMatrix;
template <class T_elem>
Matrix< T_elem> calculateInvMatrix (Matrix< T_elem> &A)
{
	Matrix< T_elem> L, U, P, Q;

	int isSingular = LUP (A, L, U, P, Q);

	if (isSingular)
	{
		return mmatrix::GetNullMatrix();
	}

	return mmatrix::CalculateInvertibleMatrix(L, U, P, Q);
}

static int count = 0;

//return x in A*x = b, by LUPQ decomposition
template <class T_elem>
Matrix< T_elem> solveSole (Matrix< T_elem> &A, Matrix< T_elem> &b/*, int &AMOUNT for counting*/)
{
	Matrix< T_elem> L, U, P, Q;

	int AMOUNT = 0;
	LUP (A, L, U, P, AMOUNT, Q);

//#if !HOME
//	string a;
//	if (count > 10)
//	{
//		a = count / 10 + '0';
//		a += count % 10 + '0';
//	}
//	else
//	{
//		a = count + '0';
//	}
//	a += ".txt";
//	ofstream fout(a);
//
//	fout << A << endl << "----------" << endl;
//	fout << L << endl << "----------" << endl;
//	fout << U << endl << "----------" << endl;
//	fout << P << endl << "----------" << endl;
//	fout << Q << endl << "----------" << endl;
//
//	fout << P*A*Q << endl << "----------" << endl;
//	fout << L*U << endl << "----------" << endl;
//
//
	Matrix< T_elem> ans = ResolveSOLE_LUP(L, U, P, b, AMOUNT, Q);
//
//	fout << ans << endl << "----------" << endl;
//	fout << A*ans << endl << "----------" << endl;
//	fout << b << endl << "---------------" << endl;
//
//	fout.close();
//
//	count++;
//#endif

	return ans;
}

//return [m x n] matrix fills with value
template <class T_elem>
Matrix< T_elem> values (int n, int m, T_elem value)
{
	Matrix< T_elem> matrix(n, m);

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < m; ++j)
		{
			matrix[i][j] = value;
		}
	}

	return matrix;
}

//return vector-answer with multiplication U and Q
template <class T_elem>
Matrix< T_elem> ResolveSOLE_LUP_legacy (Matrix< T_elem> L, Matrix< T_elem> U, Matrix< T_elem> &P, Matrix< T_elem> b, Matrix< T_elem>& Q = GetNullMatrix())
{
	//firstly we must solve Ly = Pb, so...
	//P*b

	b = P*b;
	int n = b.getN();
	//������ ������ ������ ��������
	Matrix < T_elem> y(n, 1);

	y[0][0] = b[0][0];
	for (int i = 1; i < n; ++i)
	{
		y[i][0] = b[i][0];
		for (int j = 0; j < i; ++j)
		{
			y[i][0] -= y[j][0]*L[i][j];
		}
	}

	//secondly we must solve Ux = y by backward substitution
	int *order;
	order = new int[n];
	for (int i = 0; i < n; ++i)
	{
		order[i] = i;
	}
	if (! (Q == GetNullMatrix()) )
	{
		Matrix< double> order_vector(1, n);
		for (int i = 0; i < n; ++i)
		{
			order_vector[0][i] = i;
		}
		order_vector = order_vector*Q.transpose();
		for (int i = 0; i < n; ++i)
		{
			order[ static_cast < int> (order_vector[0][i])] = i;
		}
		U = U*Q.transpose();
	}

	Matrix < T_elem> x(n, 1);
	int t = order[n - 1];
	//cout << U << endl << "----------" << endl;
	x[ t][0] = y[n - 1][0] / U[ n - 1][ t];
	for (int i = n - 2; i >= 0; --i)
	{
		t = order[i];
		for (int j = n - 1; j > i; --j)
		{
			y[i][0] -= x[ order[j]][ 0]*U[ i][ order[j]];
		}
		x[ t][0] = y[i][0] / U[ i][t];
	}

	delete []order;
	return x;
}

template< typename T_elem> 
Matrix< T_elem> solveSoleGaus ( Matrix < T_elem> matrix, Matrix< T_elem> b)
{
	T_elem pivotValue = 0;
	int n = matrix.getN();
	for( int i = 0; i < n; i++ ) {
		pivotValue = 0;
		int pivotRow = -1, pivotCol = -1;

		for (int row = i; row < n; ++row)
		{
			for (int col = i; col < n; ++col)
			{
				if ( abs(matrix[row][col]) > pivotValue) {
					pivotValue = abs( matrix[row][col]);
					pivotRow = row;
					pivotCol = col;
				}
			}
		}

		if (pivotValue == 0)
		{
			cout << "Matrix is singular__solveSoleGaus" << endl;
			return 0;
		}

		matrix.SwapRows(pivotRow, i);
		matrix.SwapCols(pivotCol, i);

		for (int j = i + 1; j < n; ++j)
		{
			matrix[i][j] /= matrix[i][i];
		}
		matrix[i][i] = 1;

		for (int j = i + 1; j < n; ++j)
		{
			for (int k = i + 1; k < n; ++k)
			{
				matrix[j][k] -= matrix[j][i]*matrix[i][k];
			}
			b[j][0] -= matrix[j][i]*b[i][0];
			matrix[j][i] = 0;
		}
	}

	//secondly we must solve Ux = y by backward substitution
	Matrix < T_elem> x(n, 1);

	//x[ n - 1][0] = b[n - 1][0] / matrix[ n - 1][ n - 1];
	for (int i = n - 1; i >= 0; --i)
	{
		//cout << U << endl << "----------" << endl;

		//cout << x << endl << "----------" << endl;
		//cout << y << endl << "----------" << endl;
		for (int j = n - 1; j > i; --j)
		{
			b[i][0] -= x[ j][ 0]*matrix[ i][ j];
			//cout << y << endl << "----------" << endl;
		}
		x[ i][0] = b[i][0] / matrix[ i][ i];
	}

	return x;
}

}